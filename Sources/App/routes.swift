import Vapor

/// Register your application's routes here.

public func routes(_ router: Router) throws {
    
    let authController = AuthController()
    router.post("login",
                use: authController.login)
    router.post("logout",
                use: authController.logout)
    router.post("registerUser",
                use: authController.registration)
    router.post("changeUserData",
                use: authController.changeUserData)
    
    let productController = ProductController()
    router.post("catalogData",
                use: productController.productList)
    router.post("getGoodById",
                use: productController.product)
    
    let reviewController = ReviewController()
    router.post("addReview",
                use: reviewController.addReview)
    router.post("approveReview",
                use: reviewController.approveReview)
    router.post("removeReview",
                use: reviewController.removeReview)
    
    let basketController = BasketController()
    router.post("addToBasket",
                use: basketController.addToBasket)
    router.post("deleteFromBasket",
                use: basketController.deleteFromBasket)
    router.post("getBasket",
                use: basketController.getBasket)
}

