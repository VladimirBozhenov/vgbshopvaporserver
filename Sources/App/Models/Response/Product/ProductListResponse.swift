//
//  ProductListResponse.swift
//  App
//
//  Created by Vladimir Bozhenov on 09/08/2019.
//

import Vapor

struct ProductListResponse: Content {
    var result: Int?
    var products: [Product]?
    var errorMessage: String?
}
