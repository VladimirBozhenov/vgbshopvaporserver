//
//  ProductResponse.swift
//  App
//
//  Created by Vladimir Bozhenov on 09/08/2019.
//

import Vapor

struct ProductResponse: Content {
    var result: Int
    var product_name: String?
    var product_price: Int?
    var product_description: String?
    var errorMessage: String?
}
