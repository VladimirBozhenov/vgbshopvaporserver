//
//  LogoutResponse.swift
//  App
//
//  Created by Vladimir Bozhenov on 09/08/2019.
//

import Vapor

struct LogoutResponse: Content {
    var result: Int
    var errorMessage: String?
}

