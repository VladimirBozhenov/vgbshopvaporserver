//
//  LoginResponse.swift
//  App
//
//  Created by Vladimir Bozhenov on 09/08/2019.
//

import Vapor

struct LoginResponse: Content {
    var result: Int
    var user: User?
    var errorMessage: String?
}
