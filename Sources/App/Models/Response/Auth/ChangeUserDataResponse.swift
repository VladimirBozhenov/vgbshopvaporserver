//
//  ChangeUserDataResponse.swift
//  App
//
//  Created by Vladimir Bozhenov on 09/08/2019.
//

import Vapor

struct ChangeUserDataResponse: Content {
    var result: Int
    var userMessage: String?
    var errorMessage: String?
}
