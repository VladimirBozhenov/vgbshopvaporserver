//
//  DeleteFromBasketResponse.swift
//  App
//
//  Created by Vladimir Bozhenov on 18/08/2019.
//

import Vapor

struct DeleteFromBasketResponse: Content {
    var result: Int
    var errorMessage: String?
}
