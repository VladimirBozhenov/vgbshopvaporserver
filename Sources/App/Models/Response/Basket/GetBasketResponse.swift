//
//  GetBasketResponse.swift
//  App
//
//  Created by Vladimir Bozhenov on 18/08/2019.
//

import Vapor

struct GetBasketResponse: Content {
    var result: Int?
    var amount: Int?
    var countGoods: Int?
    var contents: [Product]?
    var errorMessage: String?
}

