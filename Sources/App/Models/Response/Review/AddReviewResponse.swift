//
//  AddReviewResponse.swift
//  App
//
//  Created by Vladimir Bozhenov on 14/08/2019.
//

import Vapor

struct AddReviewResponse: Content {
    var result: Int
    var userMessage: String?
    var errorMessage: String?
}
