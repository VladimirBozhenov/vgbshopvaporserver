//
//  RemovrReviewReqsponse.swift
//  App
//
//  Created by Vladimir Bozhenov on 14/08/2019.
//

import Vapor

struct RemoveReviewResponse: Content {
    var result: Int
    var errorMessage: String?
}
