//
//  ProductRequest.swift
//  App
//
//  Created by Vladimir Bozhenov on 09/08/2019.
//

import Vapor

struct ProductRequest: Content {
    var id_product: Int
}
