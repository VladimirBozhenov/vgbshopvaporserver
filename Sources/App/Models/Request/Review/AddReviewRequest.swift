//
//  AddReviewRequest.swift
//  App
//
//  Created by Vladimir Bozhenov on 14/08/2019.
//

import Vapor

struct AddReviewRequest: Content {
    var id_user: Int
    var text: String
}
