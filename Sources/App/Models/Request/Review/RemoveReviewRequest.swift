//
//  RemoveReviewRequest.swift
//  App
//
//  Created by Vladimir Bozhenov on 14/08/2019.
//

import Vapor

struct RemoveReviewRequest: Content {
    var id_comment: Int
}
