//
//  GetBasketRequest.swift
//  App
//
//  Created by Vladimir Bozhenov on 18/08/2019.
//

import Vapor

struct GetBasketRequest: Content {
    var id_user: Int
}
