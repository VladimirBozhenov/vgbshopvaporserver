//
//  AddToBasketRequest.swift
//  App
//
//  Created by Vladimir Bozhenov on 18/08/2019.
//

import Vapor

struct AddToBasketRequest: Content {
    var id_product: Int
    var quantity: Int
}
