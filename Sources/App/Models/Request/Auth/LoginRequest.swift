//
//  LoginRequest.swift
//  App
//
//  Created by Vladimir Bozhenov on 09/08/2019.
//

import Vapor

struct LoginRequest: Content {
    var username: String
    var password: String
}
