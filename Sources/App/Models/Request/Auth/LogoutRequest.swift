//
//  LogoutRequest.swift
//  App
//
//  Created by Vladimir Bozhenov on 09/08/2019.
//

import Vapor

struct LogoutRequest: Content {
    var id_user: Int
}

