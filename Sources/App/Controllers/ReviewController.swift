//
//  ReviewController.swift
//  App
//
//  Created by Vladimir Bozhenov on 14/08/2019.
//

import Vapor

class ReviewController {
    func addReview(_ req: Request) throws -> Future<AddReviewResponse> {
        return try req.content.decode(AddReviewRequest.self).map { request in
            if request.id_user == 123 && request.text == "Текст отзыва" {
                return AddReviewResponse(result: 1,
                                         userMessage: "Your review was added",
                                         errorMessage: nil)
            }
            return AddReviewResponse(result: 0,
                                     userMessage: nil,
                                     errorMessage: "Add review error")
        }
    }
    
    func approveReview(_ req: Request) throws -> Future<ApproveReviewResponse> {
        return try req.content.decode(ApproveReviewRequest.self).map { request in
            if request.id_comment == 123 {
                return ApproveReviewResponse(result: 1,
                                             errorMessage: nil)
            }
            return ApproveReviewResponse(result: 0,
                                         errorMessage: "Remove review error")
        }
    }
    
    func removeReview(_ req: Request) throws -> Future<RemoveReviewResponse> {
        return try req.content.decode(RemoveReviewRequest.self).map { request in
            if request.id_comment == 123 {
                return RemoveReviewResponse(result: 1,
                                            errorMessage: nil)
            }
            return RemoveReviewResponse(result: 0,
                                        errorMessage: "Remove review error")
        }
    }
}
