//
//  AuthController.swift
//  App
//
//  Created by Vladimir Bozhenov on 09/08/2019.
//

import Vapor

class AuthController {
    func login(_ req: Request) throws -> Future<LoginResponse> {
        return try req.content.decode(LoginRequest.self).map { request in
            if request.username == "Somebody"
                && request.password == "mypassword" {
                let user = User(id_user: 123,
                                user_login: "geekbrains",
                                user_name: "John",
                                user_lastname: "Doe")
                return LoginResponse(result: 1,
                                     user: user,
                                     errorMessage: nil)
            }
            return LoginResponse(result: 0,
                                 user: nil,
                                 errorMessage: "Login error")
        }
    }
    
    func logout(_ req: Request) throws -> Future<LogoutResponse> {
        return try req.content.decode(LogoutRequest.self).map { request in
            if request.id_user == 123 {
                return LogoutResponse(result: 1,
                                      errorMessage: nil)
            }
            return LogoutResponse(result: 0,
                                  errorMessage: "Logout error")
        }
    }
    
    func registration(_ req: Request) throws -> Future<RegistrationResponse> {
        return try req.content.decode(RegistrationRequest.self).map { request in
            if request.id_user == 123
                && request.username == "Somebody"
                && request.password == "mypassword"
                && request.email == "some@some.ru"
                && request.gender == "m"
                && request.credit_card == "9872389-2424-234224-234"
                && request.bio == "This is good!" {
                return RegistrationResponse(result: 1,
                                            userMessage: "Registration successful",
                                            errorMessage: nil)
            }
            return RegistrationResponse(result: 0,
                                        userMessage: nil,
                                        errorMessage: "Login error")
        }
    }
    
    func changeUserData(_ req: Request) throws -> Future<ChangeUserDataResponse> {
        return try req.content.decode(ChangeUserDataRequest.self).map { request in
            if request.id_user == 123
                && request.username == "Somebody"
                && request.password == "mypassword"
                && request.email == "some@some.ru"
                && request.gender == "m"
                && request.credit_card == "9872389-2424-234224-234"
                && request.bio == "This is good!" {
                return ChangeUserDataResponse(result: 1,
                                              userMessage: "Data changed successful",
                                              errorMessage: nil)
            }
            return ChangeUserDataResponse(result: 0,
                                          userMessage: nil,
                                          errorMessage: "Change user data error")
        }
    }
}
