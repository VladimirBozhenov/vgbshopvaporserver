//
//  BasketController.swift
//  App
//
//  Created by Vladimir Bozhenov on 18/08/2019.
//

import Vapor

class BasketController {
    func addToBasket(_ req: Request) throws -> Future<AddToBasketResponse> {
        return try req.content.decode(AddToBasketRequest.self).map { request in
            if request.id_product == 123 && request.quantity == 1 {
                return AddToBasketResponse(result: 1,
                                           errorMessage: nil)
            }
            return AddToBasketResponse(result: 0,
                                       errorMessage: "Add to basket error")
        }
    }
    
    func deleteFromBasket(_ req: Request) throws -> Future<DeleteFromBasketResponse> {
        return try req.content.decode(DeleteFromBasketRequest.self).map { request in
            if request.id_product == 123 {
                return DeleteFromBasketResponse(result: 1,
                                                errorMessage: nil)
            }
            return DeleteFromBasketResponse(result: 0,
                                            errorMessage: "Delete from basket error")
        }
    }
    
    func getBasket(_ req: Request) throws -> Future<GetBasketResponse> {
        return try req.content.decode(GetBasketRequest.self).map { request in
            if request.id_user == 1 {
                let contents = [Product(id_product: 123,
                                        product_name: "Ноутбук",
                                        price: 45600,
                                        product_description: nil,
                                        quantity: 1),
                                Product(id_product: 456,
                                        product_name: "Мышка",
                                        price: 1000,
                                        product_description: nil,
                                        quantity: 1)]
                return GetBasketResponse(result: nil,
                                         amount: 46600,
                                         countGoods: 2,
                                         contents: contents,
                                         errorMessage: nil)
            }
            return GetBasketResponse(result: 0,
                                     amount: nil,
                                     countGoods: nil,
                                     contents: nil,
                                     errorMessage: "Get user's basket error")
        }
    }
}
