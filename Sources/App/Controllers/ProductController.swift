//
//  ProductController.swift
//  App
//
//  Created by Vladimir Bozhenov on 09/08/2019.
//

import Vapor

class ProductController {
    func product(_ req: Request) throws -> Future<ProductResponse> {
        return try req.content.decode(ProductRequest.self).map { request in
            if request.id_product == 123 {
                return ProductResponse(result: 1,
                                       product_name: "Ноутбук",
                                       product_price: 45600,
                                       product_description: "Мощный игровой ноутбук",
                                       errorMessage: nil)
            }
            return ProductResponse(result: 0,
                                   product_name: nil,
                                   product_price: nil,
                                   product_description: nil,
                                   errorMessage: "Product error")
        }
    }
    
    func productList(_ req: Request) throws -> Future<ProductListResponse> {
        return try req.content.decode(ProductListRequest.self).map { request in
            if request.page_number == 1
                && request.id_category == 1 {
                let products = [Product(id_product: 123,
                                        product_name: "Ноутбук",
                                        price: 45600,
                                        product_description: nil,
                                        quantity: nil),
                                Product(id_product: 456,
                                        product_name: "Мышка",
                                        price: 1000,
                                        product_description: nil,
                                        quantity: nil)]
                return ProductListResponse(result: nil,
                                           products: products,
                                           errorMessage: nil)
            }
            return ProductListResponse(result: 0,
                                       products: nil,
                                       errorMessage: "Product list error")
        }
    }
}
