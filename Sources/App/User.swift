//
//  User.swift
//  App
//
//  Created by Vladimir Bozhenov on 09/08/2019.
//

import Vapor

struct User: Content {
    var id_user: Int
    var user_login: String
    var user_name: String
    var user_lastname: String
}
