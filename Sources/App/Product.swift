//
//  Product.swift
//  App
//
//  Created by Vladimir Bozhenov on 09/08/2019.
//

import Vapor

struct Product: Content {
    var id_product: Int?
    var product_name: String
    var price: Double
    var product_description: String?
    var quantity: Int?
}
